from django.contrib import admin
from tasks.models import Task


# list_display controls which model attributes are visible in the admin panel.
# These are entirely up to me (but probably have a convention)


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ("name", "id")
