from django.forms import ModelForm
from projects.models import Project


# Form to create a project:
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ("name", "description", "owner")
